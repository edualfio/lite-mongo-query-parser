### Using lite-mongo-query-parser ###

Converts a request with params (mainly a brunch of values from a single param) 

**Example:**

    localhost:4400/API/list?param1=foo,bar&param2=foo

**Query object result:**

    { param1: { '$in': [ 'foo', 'bar' ] }, param2: [ 'foo' ] }


### CODING ###

```
#!javascript

var lmqp = require('lite-mongo-query-parser');

MODEL.find(lmqp(req.query)).exec((err, object) => {
	if(!object) {
		res.status(404).send({message: 'No object'});	
	} else {
		res.status(200).send({object: object});	
	}
})

```