lmqp = (q) => {
	const query = {}
		
	Object.keys(q).map(k => {
		const vArr = q[k].split(',')
		if (vArr.length > 1) {
			query[k] = { '$in': vArr }
		} else {
			k !== '' ? query[k] = vArr : null
		}
	})

	return query;
}

module.exports = lmqp